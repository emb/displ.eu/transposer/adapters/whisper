# import the baseclass for the adapter
# this class loads parses the config
# file at config_path
import base_adapter

# import the modules you need for your
# adapter implementation
import typing
import logging


import json

# import the modules you need for your
# adapter implementation
import os
import requests
from requests.exceptions import HTTPError
import sys
import urllib.request

from collections import Counter


import mimetypes

import time
#may not need it in future whn reading file from url
import shutil
from cerberus import Validator

import zipfile
import os
import json
import uuid
from dotenv import load_dotenv



URL = 'url'
SOURCE_ID = 'source_id'
LANGUAGE = 'language'

PAYLOAD_SCHEMA = {
    URL: {'type': 'string', 'required': True},
    SOURCE_ID: {'type': 'string'},
    LANGUAGE: {'type': 'string', 'required': True} # we need this for now because cba needs it
}


LANGUAGES = 'languages'
MODEL_NAME = 'model_name'
API_KEY_CBA = 'api_key_cba'
TRANSCRIPTION_STATUS_TOPIC = 'transcription_status_topic'
DOWNLOAD_FOLDER_PATH = 'download_files'
API_DOMAIN_CBA = "api_domain_cba"
TRANSCRIBE_AND_TRANSLATE_COLLECTION = "transcribe_and_translate"

class MediaTypeNotSupportedException(Exception):
    pass

class Adapter(base_adapter.BaseAdapter):

    def __init__(self, connector: typing.Any, config_path: str | None = None) -> None:
        super(Adapter, self).__init__(connector, config_path)
        print('Whisper Adapter ... config:', self.config)
        
        self.languages = self.config.get('whisper_adapter').get(LANGUAGES)
        self.model_name = self.config.get('whisper_adapter').get(MODEL_NAME)
        self.api_key_cba = self.config.get('whisper_adapter').get(API_KEY_CBA)
        self.api_domain_cba = self.config.get("whisper_adapter").get(API_DOMAIN_CBA).rstrip("/")
        self.transcription_status_topic = self.config.get('whisper_adapter').get(TRANSCRIPTION_STATUS_TOPIC)
        self.process_id = str(time.time())
        self.peertube_collection = self.set_up_database()

    def run(self, payload, msg):
        print('This is the Whisper Adapter')

        result = {
            'success': False,
        }
        source_id = None
        
        if SOURCE_ID in payload:
            source_id = payload[SOURCE_ID]
            result[SOURCE_ID] = source_id
            media_id = source_id
        else:
            media_id = str(uuid.uuid4())
        
        output = None
        self.create_or_delete_folder()

        try:
            validator = Validator(PAYLOAD_SCHEMA)
            validator.allow_unknown = True
            if validator.validate(payload) is False:
                # validator.errors could be for example {'input': ['required field'], 'format': ['unallowed value ee']}
                logging.error(f"validation error: {validator.errors}")
                return result
            url = payload[URL]
            file_path = self.get_file_path(url)


            # if not os.path.exists(file_path):
            #     raise FileNotFoundError(f"The file '{file_path}' does not exist.")

            language_id = None
            if LANGUAGE in payload and payload[LANGUAGE].lower() in self.languages:
                language_id = payload[LANGUAGE]

            media_type = self.get_media_type(file_path)

            if media_type is None:
                raise MediaTypeNotSupportedException("Provided media type is not supported. Please provide a file that is either audio or video.")

            # Creating job in CBA service
            job = self.create_job(media_id, media_type, self.model_name, url, language_id)

            # Waiting for job to finish
            while True:
                job_data = self.get_job(job["id"])
                if job_data["status"] == "DONE":
                    break
                elif job_data["status"] == "FAILED":
                    logging.error(job_data)
                    raise HTTPError("Job could not be processed on service.cba.media.")
                else:
                    time.sleep(10)
            
            output = self.download_job(job["id"])
            for segment in output['segments']:
                if 'tokens' in segment:
                    del segment['tokens']
            
            output = json.dumps(output)
            result['success'] = True
            
            if source_id is not None:
                logging.info(f"source_id: {source_id}")
                logging.info(f"result before updating collection: {result}")
                
                doc = self.get_data_by_source_id(source_id)
                logging.info(f"doc: {doc}")
                doc["output"] = output
                self.peertube_collection.update_one({'source_id':source_id}, {"$set": doc}, upsert=False)
            else:
                logging.info(f"source_id is None")

        except Exception:
            logging.exception('Exception thrown')

        # Removing folder of job
        self.create_or_delete_folder()
        result[URL] = payload[URL]
        # TODO get language (if it was passed) from job!
        result['language'] = language_id

        return result

    def set_up_database(self):
        load_dotenv()
        config = {
            'username': os.getenv('MONGO_USERNAME'),
            'password': os.getenv('MONGO_PASSWORD'),
            'host': os.getenv('MONGO_HOST'),
            'database': os.getenv('MONGO_DATABASE'),
            'port': int(os.getenv('MONGO_PORT'))
        }
        db = self.connector.database['mongo'].MongoDB(config)
        db.connect()
        collection = db.get_collection(TRANSCRIBE_AND_TRANSLATE_COLLECTION)
        return collection

    def get_data_by_source_id(self, source_id):
        query = {"source_id": source_id}
        source_id_doc = list(self.peertube_collection.find(query))[0]
        return source_id_doc

    # def download_file(self, url):
    #     file_path = None
    #     with requests.get(url, stream=True) as response:
    #         response.raise_for_status()
            
    #         with open(file_path, 'wb') as file:
    #             for chunk in response.iter_content(chunk_size=1024):
    #                 if chunk:
    #                     file.write(chunk)
    #     return file_path
    
    # Function to get path of file in download directory
    def get_file_path(self, url):
        file_name = os.path.basename(url)  # Extract the last part of the URL as the file name
        return os.path.join(self.get_folder_path(), file_name)

    # Function to get path of folder in download directory
    def get_folder_path(self):
        return os.path.join(DOWNLOAD_FOLDER_PATH, self.process_id)


    def create_or_delete_folder(self):
        folder_path = os.path.join(DOWNLOAD_FOLDER_PATH, self.process_id)
        if os.path.exists(folder_path):
            shutil.rmtree(folder_path)  # Remove the folder and all its content
        os.makedirs(folder_path)

    def is_video(self, filename):
        mime_type = mimetypes.guess_type(filename)[0]
        return mime_type and mime_type.startswith('video/')

    def is_audio(self, filename):
        mime_type = mimetypes.guess_type(filename)[0]
        return mime_type and mime_type.startswith('audio/')


    # Function to create job in CBA service
    def create_job(self, media_id, media_type, model, url, language):
        headers = { 
            "x-api-key": self.api_key_cba,
            "Content-type": "application/json", 
            "Accept": "application/json"
        }
        data = {
            "connector": "HTTP",
            "meta": {
                "language": language,
                "mediaId": media_id,
                "mediaFile": url,
                "mediaType": media_type,
                "model": model,
            },
            "type": "TRANSCRIBE"
        }
        response = requests.post(f"{self.api_domain_cba}/api/transcripts", json=data, headers=headers)
        response.raise_for_status()
        job = response.json()
        # Checking if job was created successfully
        if "id" not in job:
            raise HTTPError("Failed to create job on service.cba.media.")
        return job
    
        # Function to get media type of file
    def get_media_type(self, file_path):
        mime_type = mimetypes.guess_type(file_path)[0]
        if mime_type and mime_type.startswith("audio/"):
            return "AUDIO"
        elif mime_type and mime_type.startswith("video/"):
            return "VIDEO"
        else:
            return None
        
    # Function to get job data from CBA service
    def get_job(self, jobId):
        headers = { "x-api-key": self.api_key_cba }
        response = requests.get(f"{self.api_domain_cba}/api/transcripts/{jobId}", headers=headers)
        return response.json()

    # Function to get job data from CBA service
    def download_job(self, jobId):
        headers = { "x-api-key": self.api_key_cba }
        response = requests.get(f"{self.api_domain_cba}/api/downloads/{jobId}", headers=headers)
        zip_file_name = "download.zip"
        with open(zip_file_name, 'wb') as file:
            # Write the content of the response to the file
            for chunk in response.iter_content(chunk_size=8192):
                file.write(chunk)
        extract_dir = "extracted_files"
        os.makedirs(extract_dir, exist_ok=True)
        with zipfile.ZipFile(zip_file_name, 'r') as zip_ref:
            zip_ref.extractall(extract_dir)
        os.remove(zip_file_name)
        first_file = next(os.scandir(extract_dir), None)
        if first_file is not None:
            file_path = os.path.join(extract_dir, first_file.name)
            with open(file_path, 'r') as file:
                data = json.load(file)
        shutil.rmtree(extract_dir)
        
        return data
    
    def adapter_name(self):
        return 'whisper_adapter'


